"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var CRJSON;
(function (CRJSON) {
    function stringify(object, replacer, spaces) {
        var encountered = new WeakMap();
        var refCount = 0;
        var PARSE = function (item, key) {
            switch (typeof item) {
                case "number":
                case "string":
                case "boolean":
                    return item;
                case "object":
                    if (item === null) {
                        return null;
                    }
                    if (Array.isArray(item)) {
                        return item.map(function (ITM, idx) {
                            return PARSE(ITM, [key, idx.toString()].filter(function (i) { return !!i; }).join("."));
                        });
                    }
                    if (Buffer.isBuffer(item)) {
                        return JSON.parse(JSON.stringify(item));
                    }
                    if (encountered.has(item)) {
                        return encountered.get(item);
                    }
                    refCount++;
                    encountered.set(item, "$ref" + refCount + ":" + key);
                    var n_1 = {};
                    Object.keys(item)
                        .forEach(function (K) {
                        n_1[K] = PARSE(item[K], [key, K].filter(function (i) { return !!i; }).join("."));
                    });
                    return n_1;
                default:
                    return null;
            }
        };
        return JSON.stringify(PARSE(object, "#"), replacer, spaces);
    }
    CRJSON.stringify = stringify;
    function parse(str) {
        var target = JSON.parse(str);
        var PARSE = function (item) {
            switch (typeof item) {
                case "string":
                    if (item.match(/^\$ref\d+:#.*/)) {
                        var pointer_1 = target;
                        item.replace(/^\$ref\d+:#(?:\.(.*))?/, "$1")
                            .split(/\./g)
                            .filter(function (K) { return !!K; })
                            .map(function (K) {
                            if (K.match(/^\d+$/)) {
                                return parseInt(K, 10);
                            }
                            return K;
                        })
                            .forEach(function (K) {
                            pointer_1 = pointer_1[K];
                        });
                        return pointer_1;
                    }
                    else {
                        return item;
                    }
                case "object":
                    if (item === null) {
                        return null;
                    }
                    if (Array.isArray(item)) {
                        return item.map(function (ITM, idx) { return PARSE(ITM); });
                    }
                    Object.keys(item)
                        .forEach(function (K) {
                        item[K] = PARSE(item[K]);
                    });
                    return item;
                default:
                    return item;
            }
        };
        switch (typeof target) {
            case "number":
            case "string":
            case "boolean":
                return target;
            case "object":
                if (target === null) {
                    return null;
                }
                if (Array.isArray(target)) {
                    return target.map(function (ITM, idx) { return PARSE(ITM); });
                }
                Object.keys(target)
                    .forEach(function (K) {
                    target[K] = PARSE(target[K]);
                });
                return target;
            default:
                return null;
        }
    }
    CRJSON.parse = parse;
})(CRJSON = exports.CRJSON || (exports.CRJSON = {}));
//# sourceMappingURL=index.js.map