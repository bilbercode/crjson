export namespace CRJSON {
  export function stringify(obj: any, replacer?: (key, value) => any, spaces?: number): string;
  export function parse<T>(str: string): T;
}

