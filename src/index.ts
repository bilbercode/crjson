export namespace CRJSON {

  export function stringify(object: string, replacer?: (key, value) => any, spaces?: number) {

    const encountered = new WeakMap();
    let refCount = 0;

    const PARSE = (item, key) => {
      switch (typeof item) {
        case "number":
        case "string":
        case "boolean":
          return item;
        case "object":
          if (item === null) {
            return null;
          }

          if (Array.isArray(item)) {
            return item.map((ITM, idx) =>
              PARSE(ITM, [key, idx.toString()].filter(i => !!i).join(".")));
          }

          if (Buffer.isBuffer(item)) {
            return JSON.parse(JSON.stringify(item));
          }

          if (encountered.has(item)) {
            return encountered.get(item);
          }
          refCount++;
          encountered.set(item, `$ref${refCount}:${key}`);

          let n = {};
          Object.keys(item)
            .forEach(K => {
              n[K] = PARSE(item[K], [key, K].filter(i => !!i).join("."));
            });

          return n;
        default:
          return null;
      }
    };
    return JSON.stringify(PARSE(object, "#"), replacer, spaces);
  }

  export function parse<T>(str: string): T {

    let target = JSON.parse(str);


    const PARSE = (item) => {
      switch (typeof item) {
        case "string":

          if (item.match(/^\$ref\d+:#.*/)) {
            let pointer = target;
            item.replace(/^\$ref\d+:#(?:\.(.*))?/, "$1")
              .split(/\./g)
              .filter(K => !!K)
              .map(K => {
                if (K.match(/^\d+$/)) {
                  return parseInt(K, 10);
                }

                return K;
              })
              .forEach(K => {
                pointer = pointer[K];
              });
            return pointer;
          } else {
            return item;
          }
        case "object":
          if (item === null) {
            return null;
          }

          if (Array.isArray(item)) {
            return item.map((ITM, idx) => PARSE(ITM));
          }

          Object.keys(item)
            .forEach(K => {
              item[K] = PARSE(item[K]);
            });
          return item;
        default:
          return item;
      }
    };



    switch (typeof target) {
      case "number":
      case "string":
      case "boolean":
        return target;
      case "object":
        if (target === null) {
          return null;
        }

        if (Array.isArray(target)) {
          return <any>target.map((ITM, idx) => PARSE(ITM));
        }

        Object.keys(target)
          .forEach(K => {
            target[K] = PARSE(target[K]);
          });
        return target;
      default:
        return null;
    }
  }

}



